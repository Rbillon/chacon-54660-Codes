# Chacon 54660 433MHZ switch codes

### With a 433mhz radio emmiter plugged on a raspberry pi or an arduino, you can send these codes to switch on or off the selected chacon switch.

### These codes have been decoded using the chacon remote and an arduino with 433mhz receiver plugged.


Chacon 54660 codes :

| CHAN/ID  | ON           | OFF        |
| -------- |:------------:| :-----:    |
| I/1      | 1381717      |    1381716 |
| I/2      | 1394005      |    1394004 |
| I/3      | 1397077      |    1397076 |
| I/4      | 1397845      |    1397844 |
| II/1     | 4527445      |    4527444 |
| II/2     | 4539733      |    4539732 |
| II/3     | 4542805      |    4542804 |
| II/4     | 4543573      |    4543572 |
| III/1    | 5313877      |    5313876 |
| III/2    | 5326165      |    5326164 |
| III/3    | 5329237      |    5329236 |
| III/4    | 5330005      |    5330004 |
| IV/1     | 5510485      |    5510484 |
| IV/2     | 5522773      |    5522772 |
| IV/3     | 5525845      |    5525844 |
| IV/4     | 5526613      |    5526612 |
